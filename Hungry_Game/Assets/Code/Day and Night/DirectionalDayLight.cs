﻿using UnityEngine;
using System.Collections;

public class DirectionalDayLight : MonoBehaviour 
{
	private float lightIntense = 0.2f;
	public Light dirLight;
	public DayTimer dayTimer;

	public void Awake()
	{
		dayTimer = GameObject.FindObjectOfType(typeof(DayTimer)) as DayTimer;
	}

	public void Update()
	{
		dirLight.intensity = lightIntense;

		if(dayTimer.hourOfTheDay >= 3 && dayTimer.hourOfTheDay <= 15)
		{
			lightIntense += (0.05f / dayTimer.hourRate) * Time.deltaTime;
			if(lightIntense >= 0.7f)
			{
				lightIntense = 0.7f;
			}
		}

		else
		{
			lightIntense -= (0.075f / dayTimer.hourRate) * Time.deltaTime;
			if(lightIntense <= 0.2f)
			{
				lightIntense = 0.2f;
			}
		}
	}
}
