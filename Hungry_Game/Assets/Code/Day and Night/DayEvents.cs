﻿using UnityEngine;
using System.Collections;

public class DayEvents : MonoBehaviour 
{
	private DayTimer dayTimer;
	public int dayTime;
	public int nightTime;

	public event EventHandlers.MinimalHandler DayEvent = delegate { };
	public event EventHandlers.MinimalHandler NightEvent = delegate { };

	public void Awake()
	{
		dayTimer = GameObject.FindObjectOfType(typeof(DayTimer)) as DayTimer;
	}

	public void Update()
	{
		if(dayTimer.hourOfTheDay == dayTime && dayTimer.timer == 0)
		{
			//print ("Day Time");
			DayEvent();
		}

		if(dayTimer.hourOfTheDay == nightTime && dayTimer.timer <= 1)
		{
			//print ("Night Time");
			NightEvent();
		}
	}
}
