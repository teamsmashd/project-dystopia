﻿using UnityEngine;
using System.Collections;

public class BoolDayNight : MonoBehaviour 
{
	public bool dayTime;
	public bool nightTime;

	public void setDayTimeBool()
	{
		dayTime = true;
		nightTime = false;
	}

	public void setNightTimeBool()
	{
		dayTime = false;
		nightTime = true;
	}
}
