﻿using UnityEngine;
using System.Collections;

public class DayTimer : MonoBehaviour 
{
	public float timer;
	public int startHour;
	public int hourOfTheDay;
	public float hourRate;

	public void Awake()
	{
		hourOfTheDay = startHour;
	}

	public void Update()
	{
		timer += 1 * Time.deltaTime;

		if(timer >= hourRate)
		{
			hourOfTheDay ++;
			timer = 0;
		}

		if(hourOfTheDay >= 24)
		{
			hourOfTheDay = 0;
		}
	}
}
