﻿using UnityEngine;
using System.Collections;

public class LerpDayNightFunctions : MonoBehaviour 
{
	public BoolDayNight boolDayNight;
	public LerpPositionV3 lerpPositionV3;

	public void Update()
	{
		if(boolDayNight.dayTime)
		{
			lerpPositionV3.LerpPositionForward();
		}

		else 
		if(boolDayNight.nightTime)
		{
			lerpPositionV3.LerpPositionBackwards();
		}
	}
}
