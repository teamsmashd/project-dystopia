﻿using UnityEngine;
using System.Collections;

public class CameraPosition : MonoBehaviour 
{
	public Camera mainCamera;
	public GameObject cameraPosition;
	public float cameraViewSize;
	public string startPositionTag;

	private GameObject startCameraPosition;
	private float startCameraSize;


	public void Awake()
	{
		cameraPosition = GameObject.FindGameObjectWithTag(startPositionTag);
		mainCamera.orthographicSize = cameraViewSize;

		startCameraPosition = GameObject.FindGameObjectWithTag(startPositionTag);
		startCameraSize = cameraViewSize;
	}

	public void Update()
	{
		mainCamera.transform.position = cameraPosition.transform.position;
		mainCamera.orthographicSize = cameraViewSize;
	}

	public void ChangeCameraPosition(GameObject newCameraPosition, float newCameraSize)
	{
		cameraPosition = newCameraPosition;
		cameraViewSize = newCameraSize;
	}

	public void ChangeStartCamera(GameObject newStartCameraPos, float newStartCameraSize)
	{
		startCameraPosition = newStartCameraPos;
		startCameraSize = newStartCameraSize;
	}

	public void ResetCameraPosition()
	{
		cameraPosition = startCameraPosition;
		cameraViewSize = startCameraSize;
	}
}
