﻿using UnityEngine;
using System.Collections;

public class ChangeCameraPosition : MonoBehaviour 
{
	public CameraPosition cameraPosition;
	public GameObject newPosition;
	public float newCameraSize;
	public string playerTag;

	public void Awake()
	{
		cameraPosition = GameObject.FindObjectOfType(typeof(CameraPosition)) as CameraPosition;
	}

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == playerTag)
		{
			cameraPosition.ChangeCameraPosition(newPosition, newCameraSize);
		}
	}
}
