﻿using UnityEngine;
using System.Collections;

public class CheckPointStart : MonoBehaviour 
{
	public GameObject checkPointPosition;
	public GameObject thingToSpawn;
	public string checkPointTag;
	public string thingToSpawnTag;
	
	void Start()
	{
		checkPointPosition = GameObject.FindGameObjectWithTag(checkPointTag);
		thingToSpawn = GameObject.FindGameObjectWithTag(thingToSpawnTag);
		MoveToCheckPoint();
	}

	public void MoveToCheckPoint()
	{
		//GameObject clone;
		//clone = Instantiate(thingToSpawn, checkPointPosition.transform.position, checkPointPosition.transform.rotation) as GameObject;

		thingToSpawn.transform.position = checkPointPosition.transform.position;
		thingToSpawn.transform.rotation = checkPointPosition.transform.rotation;
	}
}
