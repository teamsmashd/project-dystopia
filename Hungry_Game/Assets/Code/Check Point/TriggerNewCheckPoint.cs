﻿using UnityEngine;
using System.Collections;

public class TriggerNewCheckPoint : MonoBehaviour 
{
	public CheckPoint checkPointCode;
	public GameObject newCheckPoint;
	public string tagged;
	public bool destroyObject;

	public static event EventHandlers.MinimalHandler TriggeredCheckPoint = delegate { };

	void Update()
	{
		checkPointCode = GameObject.FindObjectOfType(typeof(CheckPoint)) as CheckPoint;
	}

	void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			checkPointCode.checkPointPosition = newCheckPoint;
			TriggeredCheckPoint();
			if(destroyObject)
			{
				Destroy(gameObject);
			}
		}
	}
}
