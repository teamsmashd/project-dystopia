﻿using UnityEngine;
using System.Collections;

public class TriggerRespawn : MonoBehaviour 
{
	public CheckPoint checkPointCode;
	public CharactersFoodWater characterFoodWater;
	public string tagged;

	public static event EventHandlers.MinimalHandler SeePlayerEvent = delegate { };

	void OnTriggerEnter(Collider otherThing)
	{
		checkPointCode = GameObject.FindObjectOfType(typeof(CheckPoint)) as CheckPoint;
		characterFoodWater = GameObject.FindObjectOfType(typeof(CharactersFoodWater)) as CharactersFoodWater;
		if(otherThing.tag == tagged)
		{
			checkPointCode.MoveToCheckPoint();
			characterFoodWater.ResetHealth();
			SeePlayerEvent();
		}
	}
}
