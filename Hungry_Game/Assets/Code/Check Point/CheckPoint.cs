﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour 
{
	public GameObject checkPointPosition;
	public string checkPointTag;

	void Start()
	{
		checkPointPosition = GameObject.FindGameObjectWithTag(checkPointTag);
	}

	public void MoveToCheckPoint()
	{
		gameObject.transform.position = checkPointPosition.transform.position;
	}
}
