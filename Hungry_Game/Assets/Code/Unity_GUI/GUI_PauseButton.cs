﻿using UnityEngine;
using System.Collections;

public class GUI_PauseButton : MonoBehaviour 
{
	public Rect myButtonRectangle = new Rect (10, 10, 150, 100);
	public int numberOfButtons;
	public int spaceBetweenButtons;
	public Rigidbody player;

	void OnGUI()
	{
		for(int i = 0; i < numberOfButtons; i++)
		{
			Rect newRect = myButtonRectangle;
			newRect.x += (spaceBetweenButtons + newRect.width) * i;
			
			if(GUI.Button(new Rect (newRect), "Pause"))
			{
				if(Time.timeScale == 0)
				{
					player.isKinematic = false;
					Time.timeScale = 1;
				}

				else if(Time.timeScale == 1)
				{
					player.isKinematic = true;
					Time.timeScale = 0;
				}
			}
		}
	}
}
