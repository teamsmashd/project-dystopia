﻿using UnityEngine;
using System.Collections;

public class ColliderTurnOnOff : MonoBehaviour 
{
	public GameObject thingToTurnOnOff;
	public bool onOff;

	void OnMouseDown()
	{
		thingToTurnOnOff.SetActive(onOff);
	}
}
