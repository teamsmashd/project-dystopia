﻿using UnityEngine;
using System.Collections;

public class LabelFoodWater : MonoBehaviour 
{
	public CharactersFoodWater foodWater;

	public void Awake()
	{
		foodWater = GameObject.FindObjectOfType(typeof(CharactersFoodWater)) as CharactersFoodWater;
	}

	void OnGUI() 
	{
		GUI.Label(new Rect(10, 10, 250, 20), "Player Food Health: " + foodWater.foodHealth.ToString("F2"));
		GUI.Label(new Rect(10, 30, 250, 20), "Player Water Health: " + foodWater.waterHealth.ToString("F2"));
	}
}
