﻿using UnityEngine;
using System.Collections;

public class LabelFoodWaterCollected : MonoBehaviour 
{
	public FoodCounter foodCounter;
	public WaterCounter waterCounter;
	
	public void Awake()
	{
		foodCounter = GameObject.FindObjectOfType(typeof(FoodCounter)) as FoodCounter;
		waterCounter = GameObject.FindObjectOfType(typeof(WaterCounter)) as WaterCounter;
	}
	
	void OnGUI() 
	{
		GUI.Label(new Rect(10, 50, 150, 20), "Food: " + foodCounter.foodAmount.ToString("F0"));
		GUI.Label(new Rect(10, 70, 150, 20), "Water: " + waterCounter.waterAmount.ToString("F0"));
	}
}
