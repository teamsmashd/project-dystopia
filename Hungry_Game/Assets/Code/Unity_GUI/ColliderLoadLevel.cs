﻿using UnityEngine;
using System.Collections;

public class ColliderLoadLevel : MonoBehaviour 
{
	public string levelName;
	
	void OnMouseDown()
	{
		Application.LoadLevel(levelName);
		Time.timeScale = 1;
	}
}
