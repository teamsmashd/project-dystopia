﻿using UnityEngine;
using System.Collections;

public class GUI_RestartLevel : MonoBehaviour 
{
	public Rect myButtonRectangle = new Rect (10, 10, 150, 100);
	public int numberOfButtons;
	public int spaceBetweenButtons;
	
	void OnGUI()
	{
		for(int i = 0; i < numberOfButtons; i++)
		{
			Rect newRect = myButtonRectangle;
			newRect.x += (spaceBetweenButtons + newRect.width) * i;
			
			if(GUI.Button(new Rect (newRect), "Restart"))
			{
				Time.timeScale = 1;
				Application.LoadLevel(Application.loadedLevel);
			}
		}
	}
}
