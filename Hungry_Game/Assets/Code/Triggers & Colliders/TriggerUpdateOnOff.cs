﻿using UnityEngine;
using System.Collections;

public class TriggerUpdateOnOff : MonoBehaviour 
{
	public GameObject thingToTurnOnOff;
	public bool triggerOnOff;
	public string tagged;
	
	public void OnTriggerUpdate(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			if(triggerOnOff)
			{
				thingToTurnOnOff.SetActive(true);
			}
			
			else
			{
				thingToTurnOnOff.SetActive(false);
			}
		}
	}
}
