﻿using UnityEngine;
using System.Collections;

public class TriggerBool : MonoBehaviour 
{
	public bool inTrigger;
	public string tagged;

	public void OnTriggerStay(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			inTrigger = true;
		}
	}

	public void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			inTrigger = false;
		}
	}
}
