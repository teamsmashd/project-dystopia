﻿using UnityEngine;
using System.Collections;

public class TriggerExitOnOff : MonoBehaviour 
{
	public GameObject thingToTurnOnOff;
	public bool triggerOnOff;
	public string tagged;
	
	public void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			if(triggerOnOff)
			{
				thingToTurnOnOff.SetActive(true);
			}
			
			else
			{
				thingToTurnOnOff.SetActive(false);
			}
		}
	}
}
