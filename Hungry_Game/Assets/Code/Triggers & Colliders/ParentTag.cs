﻿using UnityEngine;
using System.Collections;

public class ParentTag : MonoBehaviour 
{
	public GameObject parentObject;
	public GameObject thingToChild;
	public string tagged;

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			thingToChild = otherThing.gameObject;
			thingToChild.transform.parent = parentObject.transform;
		}
	}
}
