﻿using UnityEngine;
using System.Collections;

public class TriggerDayNightBool : MonoBehaviour 
{
	public BoolDayNight dayNight;
	public string tagged;

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			dayNight.setDayTimeBool();
		}
	}
}
