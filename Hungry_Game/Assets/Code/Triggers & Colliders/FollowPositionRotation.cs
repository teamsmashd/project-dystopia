﻿using UnityEngine;
using System.Collections;

public class FollowPositionRotation : MonoBehaviour 
{
	public GameObject thingToFollow;
	public Vector3 offset;
	public string thingToFollowName;

	public void Awake()
	{
		thingToFollow = GameObject.Find(thingToFollowName);
	}

	public void Update()
	{
		Vector3 thingFollowing = gameObject.transform.position;
		Transform followTranform = thingToFollow.transform;

		Vector3 finalOffset = new Vector3(offset.x * followTranform.forward.x, offset.y * followTranform.forward.y, offset.z * followTranform.forward.z);
		thingFollowing = followTranform.position + finalOffset;
		gameObject.transform.position = thingFollowing;
	}
}
