﻿using UnityEngine;
using System.Collections;

public class TriggerDestroy : MonoBehaviour 
{
	public string tagged;

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			GameObject.Destroy(gameObject);
		}
	}
}
