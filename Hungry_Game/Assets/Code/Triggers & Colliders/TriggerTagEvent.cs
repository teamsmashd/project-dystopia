﻿using UnityEngine;
using System.Collections;

public class TriggerTagEvent : MonoBehaviour 
{
	public string tagged;

	public event EventHandlers.MinimalHandler TagEnteredTrigger = delegate { };

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			TagEnteredTrigger();
		}
	}
}
