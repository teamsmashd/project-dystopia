﻿using UnityEngine;
using System.Collections;

public class FollowPositionTag : MonoBehaviour 
{
	public GameObject parentObject;
	public GameObject thingToChild;
	public Vector3 offset;
	public string grabTag;
	public string playerTag;

	public KeyCode grabKey;

	/*public void Awake()
	{
		parentObject = GameObject.Find(playerTag);
	}*/
	
	public void OnTriggerStay(Collider otherThing)
	{
		if(otherThing.tag == grabTag)
		{
			thingToChild = otherThing.gameObject;
		}
	}

	public void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == grabTag)
		{
			thingToChild = null;
		}
	}

	public void Update()
	{
		if(Input.GetKey(grabKey) && thingToChild != null)
		{
			//thingToChild.rigidbody.MovePosition(parentObject.transform.position + offset);
			thingToChild.transform.parent = parentObject.transform;
			thingToChild.rigidbody.MovePosition(parentObject.transform.position);
		}

		if(Input.GetKeyUp(grabKey) && thingToChild != null)
		{
			thingToChild.transform.parent = null;
			thingToChild = null;
		}
	}

	public void GrabNull()
	{
		//thingToChild.transform.parent = null;
		thingToChild = null;
	}
}
