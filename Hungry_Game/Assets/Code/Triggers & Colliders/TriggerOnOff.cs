﻿using UnityEngine;
using System.Collections;

public class TriggerOnOff : MonoBehaviour 
{
	public GameObject thingToTurnOnOff;
	public bool triggerOnOff;
	public string tagged;

	public void OnTriggerStay(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			if(triggerOnOff)
			{
				thingToTurnOnOff.SetActive(true);
			}
			
			else
			{
				thingToTurnOnOff.SetActive(false);
			}
		}
	}
}
