﻿using UnityEngine;
using System.Collections;

public class TriggerLoadLevel : MonoBehaviour 
{
	public string SceneName;
	public string tagged1;
	public string tagged2;

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged1 || otherThing.tag == tagged2)
		{
			Application.LoadLevel(SceneName);
		}
	}
}
