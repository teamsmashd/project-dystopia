﻿using UnityEngine;
using System.Collections;

public class TriggerChangeTag : MonoBehaviour 
{
	public string tagged;
	public string newTag;
	public string oldTag;

	public void OnTriggerEnter(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			gameObject.tag = newTag;
		}
	}

	public void OnTriggerExit(Collider otherThing)
	{
		if(otherThing.tag == tagged)
		{
			gameObject.tag = oldTag;
		}
	}
}
