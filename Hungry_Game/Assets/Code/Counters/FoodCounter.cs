﻿using UnityEngine;
using System.Collections;

public class FoodCounter : MonoBehaviour 
{
	public int foodAmount;

	public void OnTriggerEnter(Collider otherThing)
	{
		FoodComponent otherFood = otherThing.gameObject.GetComponent<FoodComponent>();
		if(otherFood != null)
		{
			//print("Got Food");
			foodAmount ++;
		}
	}
}
