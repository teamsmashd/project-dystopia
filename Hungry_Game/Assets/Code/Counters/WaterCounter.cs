﻿using UnityEngine;
using System.Collections;

public class WaterCounter : MonoBehaviour 
{
	public int waterAmount;
	
	public void OnTriggerEnter(Collider otherThing)
	{
		WaterComponent otherWater = otherThing.gameObject.GetComponent<WaterComponent>();
		if(otherWater != null)
		{
			//print("Got Water");
			waterAmount ++;
		}
	}
}
