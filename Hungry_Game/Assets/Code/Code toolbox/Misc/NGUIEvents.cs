using UnityEngine;

public class NGUIEvents : MonoBehaviour
{
	public event EventHandlers.MinimalHandler OnClickedEvent = delegate { };

	public void OnClick()
	{
		OnClickedEvent();
	}
}
