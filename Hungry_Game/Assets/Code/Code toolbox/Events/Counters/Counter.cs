using UnityEngine;

public class Counter : MonoBehaviour
{
	// This one needs a ZZ for every object it needs to listen to. eg Two trigger buttons, needs two ZZ's to 'MinusOne'
	// The other StaticCounter only needs one ZZ and a list of the GO's the events are coming from.
	// NEITHER are good for runtime spawned objects like waves of zombies!

	// Variables and events
	public int count;

	public delegate void Simple();
	public event Simple CounterFinishedEvent = delegate { };


	// Functions
	public void MinusOne()
	{
		count = count - 1;

		if(count == 0)
		{
			CounterFinishedEvent();
		}
	}
}
