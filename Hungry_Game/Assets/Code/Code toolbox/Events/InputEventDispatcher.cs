using UnityEngine;

public class InputEventDispatcher : MonoBehaviour
{
	static InputEventDispatcher instance;

	public static InputEventDispatcher Instance
	{
		get
		{
			if(instance == null)
			{
				// Maybe it was manually placed in the scene
				instance = FindObjectOfType(typeof(InputEventDispatcher)) as InputEventDispatcher;

				// Nope, so create one
				if(instance == null)
				{
					GameObject go = new GameObject();
					instance = go.AddComponent<InputEventDispatcher>();
					go.name = "InputEventDispatcher singleton";
				}
			}

			return instance;
		}
	}

	public delegate void ButtonPressed(int index);

	public event ButtonPressed ButtonPressedEvent = delegate { };

	public void InvokeButtonPressedEvent(int index) { ButtonPressedEvent(index); }

	public delegate void JoystickMoved(Vector2 position);

	public event JoystickMoved JoystickMovedEvent = delegate { };
	public void InvokeJoystickMovedEvent(Vector2 position) { JoystickMovedEvent(position); }


	public event EventHandlers.MinimalHandler ActivatedJoystickEvent = delegate { };
	public void InvokeActivatedJoystickEvent() { ActivatedJoystickEvent(); }
}
