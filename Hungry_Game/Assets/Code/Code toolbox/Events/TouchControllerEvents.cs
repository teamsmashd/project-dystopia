// Seperate events from the TouchController (for stuff like AI invoking fake touches etc)

public class TouchControllerEvents
{
	// General events
	public static event EventHandlers.TouchHandlerSingle OnTouchControllerMoved = delegate { };
	public static event EventHandlers.TouchHandlerSingle OnTouchControllerBegan = delegate { };
	public static event EventHandlers.TouchHandlerSingle OnTouchControllerEnded = delegate { };
	public static event EventHandlers.TouchHandlerSingle OnTouchControllerStationary = delegate { };

	public static void InvokeTouchControllerMoving(TouchContainer touch) { OnTouchControllerMoved(touch); }

	public static void InvokeTouchControllerBegan(TouchContainer touch) { OnTouchControllerBegan(touch); }

	public static void InvokeTouchControllerEnded(TouchContainer touch) { OnTouchControllerEnded(touch); }

	public static void InvokeTouchControllerStationary(TouchContainer touch) { OnTouchControllerStationary(touch); }
	/*
		public static void InvokeTouching_TouchOnly(Touch touch, Vector2 velocity, RaycastHit raycastHit)
		{
			OnTouchControllerMoved(touch, velocity, raycastHit);
		}
	*/
}
