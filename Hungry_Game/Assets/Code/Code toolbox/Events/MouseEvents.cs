using UnityEngine;

public class MouseEvents : MonoBehaviour
{
	public event EventHandlers.MinimalHandler OnMouseDragEvent = delegate { };
	public event EventHandlers.MinimalHandler OnMouseUpEvent = delegate { };
	public event EventHandlers.MinimalHandler OnMouseDownEvent = delegate { };

	void OnMouseDown()
	{
		DebugManager.Log("MouseDown", this, 3);
		OnMouseDownEvent();
	}
	void OnMouseUp()
	{
		DebugManager.Log("MouseDown", this, 3);
		OnMouseUpEvent();
	}
	void OnMouseDrag()
	{
		DebugManager.Log("MouseDrag", this, 3);
		OnMouseDragEvent();
	}
}
