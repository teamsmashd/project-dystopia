using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class EventHandlers : MonoBehaviour
{
	public delegate void MinimalHandler();

	public delegate void BasicHandler(Object sender);

	public delegate void Handler(Object sender, EventArgs args);

	public delegate void TouchHandlerSingle(TouchContainer touch);
}