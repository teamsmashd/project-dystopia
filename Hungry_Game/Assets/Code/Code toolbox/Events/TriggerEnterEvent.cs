using System;
using UnityEngine;

public class TriggerEnterEvent : MonoBehaviour
{
	public delegate void OnTriggerEnterDelegate();
	public event OnTriggerEnterDelegate OnTriggerEnterEvent = delegate { };

	public void OnTriggerEnter(Collider other)
	{
		OnTriggerEnterEvent();
	}
}
