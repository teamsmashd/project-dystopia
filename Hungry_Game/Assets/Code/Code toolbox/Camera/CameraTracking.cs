﻿using UnityEngine;
using System.Collections;

public class CameraTracking : MonoBehaviour 
{
	public GameObject trackedObject;
	public GameObject lookAt;
	public Vector3 offset;
	private Vector3 lookAtPosition;
	public float lookAheadScalar = 1;
	public float lookAtSpeed = 1;
	public float followSpeed;

	void Start()
	{
		lookAtPosition = new Vector3();

		if (trackedObject == null)
		{
			//trackedObject = ((PlayerController)FindObjectOfType(typeof(PlayerController))).gameObject;			
		}

		if(lookAt == null)
		{
			lookAt = trackedObject;
		}

	}

	void FixedUpdate()
	{
		if (trackedObject != null)
		{
			Vector3 combinedOffset = offset;// +new Vector3(0, 0, trackedObject.rigidbody.velocity.magnitude / 3);
			transform.position = Vector3.Lerp(transform.position, trackedObject.transform.position + combinedOffset + trackedObject.rigidbody.velocity/5, followSpeed);
		}
//		transform.rotation = Quaternion.Slerp(transform.rotation, trackedObject.transform.rotation, 0.04f);
		Vector3 lookAhead = trackedObject.rigidbody.velocity*lookAheadScalar;
		lookAtPosition = Vector3.Lerp(lookAtPosition, lookAt.transform.position+lookAhead, lookAtSpeed);
		transform.LookAt(lookAtPosition);
	}
}
