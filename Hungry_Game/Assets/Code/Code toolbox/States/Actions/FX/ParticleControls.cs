using UnityEngine;
using System.Collections;

public class ParticleControls : StateBase
{
	public GameObject particleSystemGO;
//	public ParticleEmitter particleEmitter;
//	public ParticleAnimator particleAnimator;
//	public ParticleRenderer particleRenderer;

//	public bool emit;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		particleSystemGO.GetComponent<ParticleEmitter>().emit = true;
	}

	public override void OnStateExit(Object sender)
	{
		base.OnStateExit(sender);

		particleSystemGO.GetComponent<ParticleEmitter>().emit = false;
	}
}
