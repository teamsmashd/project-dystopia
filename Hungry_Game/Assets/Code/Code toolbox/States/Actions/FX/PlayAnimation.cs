using UnityEngine;

public class PlayAnimation : StateBase
{
//	public Animation animationComponent;
	public AnimationClip animationToPlay;
	public bool reverse = false;
	public float speed = 1;
	public float startPoint = 0; // This is 0 for start, 1 for end. eg 0.5 = half way
	public bool transitionOnEnd;
	public bool crossFade = false;
	public float fadeTime = 0.1f;
	public bool forcePlay = false;
	public bool autoFindAnimation = true;
	public Animation mainAnimation;
	//public bool loop;

	public override void Awake()
	{
		base.Awake();

		if(mainAnimation == null && autoFindAnimation)
			mainAnimation = rootIdentity.GetComponentInChildren<Animation>();

		/*if(mainAnimation != null)
			if(loop)
				mainAnimation[animationToPlay.name].wrapMode = WrapMode.Loop;
			else
				mainAnimation[animationToPlay.name].wrapMode = WrapMode.Default;
		else if(DebugManager.Instance.showWarnings)
			Debug.LogWarning("No Animation Component set", this);*/
	}


	// Use this for initialization
	public override void OnStateEnter(Object sender)
	{
		Play();
	}

	public void FixedUpdate()
	{
		if(forcePlay)
			if(!mainAnimation.isPlaying)
				Play();

		// Animation finished
		if(!mainAnimation.isPlaying)
			if(transitionOnEnd)
				stateManager.NextState();
	}

	void Play()
	{
		if(mainAnimation != null)
		{
			AnimationState animationState = mainAnimation[animationToPlay.name];
			if(reverse)
			{
				animationState.time = (1 - startPoint)*animationState.length;
				animationState.speed = -speed;
			}
			else
			{
				animationState.time = startPoint * animationState.length;
				animationState.speed = speed;
			}

			
			if(crossFade)
				if(fadeTime < 0)
					mainAnimation.CrossFade(animationToPlay.name);
				else
					mainAnimation.CrossFade(animationToPlay.name, fadeTime);
			else
				mainAnimation.Play(animationToPlay.name);
		}
	}
}
