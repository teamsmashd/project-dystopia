using UnityEngine;

public class Impulse : StateBase
{
	public Vector3 impulseDirection;
	public float forceScalar;
	public bool localForce = true;

	public bool findNearestRigidbody = true;
	public Rigidbody useRigidbody;

	public override void Awake()
	{
		base.Awake();

		if(findNearestRigidbody)
			useRigidbody  = ToolBox.GetComponentInParents<Rigidbody>(transform);
	}

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		if(useRigidbody != null)
			if(localForce)
				useRigidbody.AddRelativeForce(impulseDirection.normalized * forceScalar, ForceMode.Impulse);

			else
				useRigidbody.AddForce(impulseDirection.normalized * forceScalar, ForceMode.Impulse);
	}
}
