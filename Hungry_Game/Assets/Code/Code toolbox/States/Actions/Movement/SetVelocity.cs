using UnityEngine;

public class SetVelocity : StateBase
{
	public bool findNearestRigidbody = true;
	public Rigidbody useRigidbody;
	public Vector3 newVelocity;
	public bool setX = false;
	public bool setY = true;
	public bool setZ = false;
	public bool useLocalDirection = true;

	public override void Awake()
	{
		base.Awake();

		if(findNearestRigidbody)
			useRigidbody = ToolBox.GetComponentInParents<Rigidbody>(transform);
	}
	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		// Maintain speeds on axis that you don't want changed. eg stop falling, but keep forward momentum
		if(useRigidbody != null)
		{
			if(useLocalDirection)
				useRigidbody.velocity = useRigidbody.rotation*newVelocity;
			else
				useRigidbody.velocity = newVelocity;

			if (!setX)
				newVelocity.x = useRigidbody.velocity.x;
			if (!setY)
				newVelocity.y = useRigidbody.velocity.y;
			if (!setZ)
				newVelocity.z = useRigidbody.velocity.z;
		}
	}
}
