using UnityEngine;

public class TransitionOnSpeed : StateBase
{
	public float delayBeforeTesting = 0.5f;
	public float lowSpeed;
	public StateManager lowSpeedState;
	public float highSpeed;
	public StateManager highSpeedState;
	public bool findNearestRigidbody = true;
	public Rigidbody useRigidbody;
	public float timer;
	public float velocity;

	public override void Awake()
	{
		base.Awake();

		if(findNearestRigidbody)
			useRigidbody = ToolBox.GetComponentInParents<Rigidbody>(transform);
	}

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		timer = 0;
	}

	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);

		velocity = useRigidbody.velocity.sqrMagnitude;
		timer += Time.deltaTime;

		if(timer > delayBeforeTesting)
			if(lowSpeedState != null && velocity < lowSpeed)
				LowSpeedStateChange();
			else if(highSpeedState != null && velocity > highSpeed)
				HighSpeedStateChange();
	}

	public override void OnStateExit(Object sender)
	{
		base.OnStateExit(sender);

		timer = 0;
	}

	// Functions so events (ZZSigSlot) can run them
	public void HighSpeedStateChange() { stateManager.ChangeState(highSpeedState); }

	public void LowSpeedStateChange() { stateManager.ChangeState(lowSpeedState); }
}
