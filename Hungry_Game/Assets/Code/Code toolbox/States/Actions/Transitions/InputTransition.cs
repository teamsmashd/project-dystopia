using UnityEngine;

public class InputTransition : StateBase
{
	public int buttonIndex;
	public float tooEarlyTimeOut = -1;
	public bool resetIfTooEarly = true;
	public bool disableIfTooEarly = true;
	public float resetTimeOut = -1;
	public bool resetIfTooLate = true;
	public bool disableIfTooLate = true;
	public bool resetIfWrongInput = true;
	public bool disableIfWrongInput = true;
	public StateManager nextState;
	public StateManager resetState;

	float enteredStateTime;
	float timeSinceStateEnter;

	public bool disabled;

	InputTransition[] inputTransitions;


	public override void Awake()
	{
		base.Awake();

		// Need to check with neighbours if I'm allowed to force the stateChange reset (this is mostly for branching decisions)
		inputTransitions = GetComponents<InputTransition>();
	}

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		InputEventDispatcher.Instance.ButtonPressedEvent += Testbuttons_ButtonPressedEvent;
		disabled = false;
		enteredStateTime = Time.timeSinceLevelLoad;
	}

	public override void OnStateExit(Object sender)
	{
		base.OnStateExit(sender);

		disabled = false;
		InputEventDispatcher.Instance.ButtonPressedEvent -= Testbuttons_ButtonPressedEvent;
	}

	// Activate
	void Testbuttons_ButtonPressedEvent(int index)
	{
		if(disabled)
			return;

		// Right button pressed
		if(index == buttonIndex)

			// To early so cancel combo
			if(timeSinceStateEnter < tooEarlyTimeOut)
			{
				if(resetIfTooEarly && resetState != null)
					stateManager.ChangeState(resetState);
				else if(disableIfTooEarly)
					disabled = true;
			}
			else if(nextState != null) // Not too early
				stateManager.ChangeState(nextState);
			else
				stateManager.NextState();
		else // Wrong button pressed
			
			// Any other input resets the state. Only if it SHOULD be reset though (eg the initial state shouldn't be effected)
			// Eg if you start a different combo don't let this one finish
			if(stateManager.initialState && (resetIfWrongInput || disableIfWrongInput))
				DebugManager.LogWarning("This is an inital state set to reset/disable on wrong input. You might not want that : " + this, this, 2);

		// Reset state
		if(resetIfWrongInput && resetState != null)
			stateManager.ChangeState(resetState);
		else if(disableIfWrongInput)
			disabled = true;
		/*
			InputTransition[] inputTransitions = GetComponents<InputTransition>();
			// I'm the only Input so I can fully reset
			bool isOnlyOneActive = true;
			foreach(InputTransition inputTransition in inputTransitions)
				if(!inputTransition.disabled)
				{
					isOnlyOneActive = false;
					break;
				}

			if(isOnlyOneActive)
			{
				if(resetIfWrongInput && resetState != null)
					stateManager.ChangeState(resetState);
			}
			else
				disabled = true;
*/
	}

	// Reset combo conditions
	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);

		// If ALL the InputTransistions are disabled then force a reset stateChange.
		bool allDisabled = true;
		foreach (InputTransition inputTransition in inputTransitions)
			if (!inputTransition.disabled)
				allDisabled = false;
		if (allDisabled)
			stateManager.ChangeState(resetState);

		// No resetState assigned, no need for timers this time
		if(resetState == null || disabled)
			return;

		timeSinceStateEnter = Time.timeSinceLevelLoad - enteredStateTime;

		// Too late so cancel combo;
		if(timeSinceStateEnter > resetTimeOut)
		{
			// Am I the biggest timeout time here? If I am ONLY THEN should I change the whole state. Otherwise let the others take care of it
			float biggestTimeout = 0;
			InputTransition inputTransitionWithBiggestTimeOut = null;
			foreach(InputTransition inputTransition in inputTransitions)
				if(inputTransition.resetTimeOut > biggestTimeout)
				{
					biggestTimeout = inputTransition.resetTimeOut;
					inputTransitionWithBiggestTimeOut = inputTransition;
				}

			// I'm the biggest so I'm allowed to change state
			// (OR there's multiple with the same value so it'll just pick the first one)
			if(resetIfTooLate && inputTransitionWithBiggestTimeOut == this)
				stateManager.ChangeState(resetState);
			else if(disableIfTooLate)
				disabled = true; // Disable (not actually turn off GO because other InputTransistions are still waiting to timeout
		}
	}
}
