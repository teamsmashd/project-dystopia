using UnityEngine;
using System.Collections;

public class OutputTransitionSimple : StateBase
{
	public KeyCode key;
	public StateManager nextState;

	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);

		if(Input.GetKeyUp(key))
		{
			stateManager.ChangeState(nextState);
		}
	}
}
