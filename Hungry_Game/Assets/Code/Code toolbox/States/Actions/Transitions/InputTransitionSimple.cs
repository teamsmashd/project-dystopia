using UnityEngine;

public class InputTransitionSimple : StateBase
{
	public KeyCode key;
	public StateManager nextState;

	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);

		if(Input.GetKeyDown(key))
		{
			stateManager.ChangeState(nextState);
		}
	}
}
