using System.Collections;
using UnityEngine;

public class TransitionToNextStateDelayed : StateBase
{
	public float delay;
    public bool autorun;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		Debug.LogWarning("This component has been replaced by 'TransitionToState.cs'");

		if (autorun)
        {
			stateManager.NextState(delay);
        }
    }
}
