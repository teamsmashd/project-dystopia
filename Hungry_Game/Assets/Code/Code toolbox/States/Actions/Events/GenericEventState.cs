using UnityEngine;

public class GenericEventState : StateBase
{
	public event EventHandlers.MinimalHandler Activate = delegate { };
	public bool sendEveryUpdate = false;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		if(!sendEveryUpdate)
			Activate();
	}

	public override void OnStateUpdate(Object sender)
	{
		base.OnStateUpdate(sender);

		if(sendEveryUpdate)
			Activate();
	}
}
