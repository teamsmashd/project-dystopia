using UnityEngine;

public class TimedDestroySpawnThing : StateBase
{
	public float time = 3;
	public bool destroyRootGO = true;
	public GameObject spawnOnDestroy;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		if (spawnOnDestroy)
		{
			Instantiate(spawnOnDestroy, transform.position, transform.rotation);
		}

		// Give states a chance to finish up (eg unsubscribe from events)
/*
		StateManager stateManager = transform.root.GetComponentInChildren<StateManager>();
		if (stateManager)
		{
			stateManager.ExitAllRunningStates();
		}
*/

		if (destroyRootGO)
		{
			Destroy(transform.root.gameObject, time);
		}
		else
			Destroy(gameObject, time);
	}
}