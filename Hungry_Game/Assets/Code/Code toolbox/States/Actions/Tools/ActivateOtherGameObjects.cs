using UnityEngine;

public class ActivateOtherGameObjects : StateBase
{
	public GameObject[] gameObjects;
	public bool disableChildrenOfGOs;
	public bool activate = true;

	public override void OnStateEnter(Object sender)
	{
		base.OnStateEnter(sender);

		foreach (GameObject go in gameObjects)
			if (disableChildrenOfGOs)
				go.SetActive(activate);
			else
				go.SetActive(activate);
	}


	public override void OnStateExit(Object sender)
	{
		base.OnStateExit(sender);

		foreach (GameObject go in gameObjects)
			if (disableChildrenOfGOs)
				go.SetActive(!activate);
			else
				go.SetActive(!activate);
	}
}
