using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// At the moment this is just used for debugging purposes. TODO: StateManager doesn't even use it for it's grouping display yet
/// </summary>
public class StateGroup : MonoBehaviour
{
	
	bool changingState;

	public void ExitAll()
	{
		foreach (var item in GetComponentsInChildren<StateManager>())
		{
			item.ExitState();
		}	
	}
	
	// For changing to any state in the group AND auto exiting any others in the group
	public void ChangeStateSolo(StateManager newState)
	{
		ExitAll();
		newState.ChangeState(newState);
	}
}