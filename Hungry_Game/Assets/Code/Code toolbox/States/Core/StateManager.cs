using System.Collections;
using UnityEngine;

///<summary>
///  This is used for individual child states of the main 'StateManager' GO:
///  - Any components you want to auto subscribe to state events should inherit from 'BaseState'. eg class CharacterDie : BaseState
///  - BaseState subscribes to these C# Events for the Enter/Exit etc events
///  - Override them instead of relying on normal Start or OnEnabled (type 'override' to get a popup). Leave the 'base.' lines as they do some things in BaseState
///  - 'TransitionToNextState' is the usual function to call if there's only one state to go to
///  - You can also call 'ChangeState' directly from another component if multiple target states needs to have decision logic
///  - C# Events
///  OnStateEnter
///  OnStateUpdate (Can be used instead of unity update, at the moment doesn't do anything more)
///  OnStateExiting (This is called while the optional delay is waiting for setting active to false
///  OnStateExit
///  - Convienience variables are (some are replicated in StateBase also)
///  stateManager	- points to the main state manager handling this group of single state GO's
///  rootIdentity	- points to the first parant GO with an 'Identity' component. The state code only uses it to grab the GO name.
///  - Handy for when states need to access a common component in the root to share variables etc. Also when you instantiate and mess with the parenting of the main GO.
/// 
///  Set 'initialState' checkbox if this is the groups starting state
///</summary>
public class StateManager : MonoBehaviour
{
	public event EventHandlers.BasicHandler OnStateEnter = delegate { };
	public event EventHandlers.BasicHandler OnStateEnterLate = delegate { };
	public event EventHandlers.BasicHandler OnStateUpdate = delegate { };
	public event EventHandlers.BasicHandler OnStateFixedUpdate = delegate { };
	public event EventHandlers.BasicHandler OnStateExiting = delegate { };
	public event EventHandlers.BasicHandler OnStateExit = delegate { };

	public bool initialState;
	//public bool alwaysActive;

	protected AllStatesManager allStatesManager;

	public StateManager nextState;
	public float delayedExitStateTime;
	public StateGroup groupParent;
	bool changingState;

	void Awake()
	{
		// Find the master stateManager. Because states are always children GO's, do a search UP the parent tree until it's found
		allStatesManager = ToolBox.GetComponentInParents<AllStatesManager>(transform.parent);
		groupParent = ToolBox.GetComponentInParents<StateGroup>(transform.parent);
	}

	// Called from main StateManger. Update loop. CHECK: Use coroutine so I can change the rate later if needed? CHECK: add 'tick' as well as 'update' to seperate graphical
	public void ManualUpdate()
	{
		OnStateUpdate(this);
	}

	// Called from main StateManger. Update loop. CHECK: Use coroutine so I can change the rate later if needed? CHECK: add 'tick' as well as 'update' to seperate graphical
	public void ManualFixedUpdate()
	{
		OnStateFixedUpdate(this);
	}

	// Simple next state, assuming most states will have just one transition state
	public void NextState()
	{
		//if(nextState.Length > 0)
		ChangeState(nextState);
	}

	// Simple next state with delay, assuming most states will have just one transition state
	public void NextState(float delay)
	{
		// If the delay value is really small, just transition without the coroutine
		if (delay > 0.0001f)
			StartCoroutine(NextStateDelay(delay));
		else
			NextState();
	}

	public IEnumerator NextStateDelay(float delay)
	{
		yield return new WaitForSeconds(delay);
		NextState();
	}

	public void ChangeState(StateManager newState, float delay)
	{
		StartCoroutine(ChangeStateDelay(newState, delay));
	}

	public IEnumerator ChangeStateDelay(StateManager newState, float delay)
	{
		yield return new WaitForSeconds(delay);
		ChangeState(newState);
	}


	// Used to prevent multiple calls to this on one frame.
	// I have to reset this variable on first enabling of the GO because there is no OnStateEnter etc
	public void OnEnable()
	{
		changingState = false;
	}

	// For changing to any state in the list
	public void ChangeState(StateManager newState)
	{
		// Just in case a single state calls this ON THE SAME FRAME (Which happens eg when you have more than 1 action responding to a button event)
		if (changingState == false)
			changingState = true;
		else
			return;

		// Disable old (me)
		//InvokeExitState();
		ExitState();

		if (newState != null)
		{
			//DebugManager.Log("ChangeState to : " + newState.name, this, 2);

			// Enable new
			newState.gameObject.SetActive(true);
			newState.EnterState();
		}
		else
			DebugManager.LogWarning("WARNING: Change state called with null StateManager!", this, 1);
		// CHECK: Should the NEXT state be in charge of calling it StateEnter stuff? Maybe on GO enable?
		// CHECK: Shouldn't that above not work? The CURRENT GO is disabled before it finished enabling the next one. I assume it waits for the next frame to do it?
	}

	public void EnterState()
	{
		OnStateEnter(this);
		OnStateEnterLate(this);
	}

	/*
		public void InvokeExitState()
		{
			Debug.Log("InvokeExitState from SSMan: " + rootIdentity.name + " : go = " + name, this);
			StartCoroutine(ExitState());
		}
	*/

	//public IEnumerator ExitState()
	public void ExitState()
	{
		OnStateExiting(this);
		//if(delayedExitStateTime>0) yield return new WaitForSeconds(delayedExitStateTime); // If delay time is zero, skip the yield because that one frame of delay even at 0 can cause problems
		OnStateExit(this);

		// This one might never be deactivated. DANGER! Really easy to get multiple conflicting states happening with this
		//if(!alwaysActive)
		gameObject.SetActive(false);
		//	yield break;
	}
}
