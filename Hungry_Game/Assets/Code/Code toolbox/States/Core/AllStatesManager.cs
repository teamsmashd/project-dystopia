using System.Collections.Generic;
using UnityEngine;

///<summary>
///  StateManager doesn't really do much. Most logic is handled in StateManager per state
///  - There are no user settable things here. If you can see it, it's just for debugging purposes
///  - It mostly just disables all the State GO's and activates the initial one(s)
///  - State GO's must be children of this
///  - Empty GO's are used for loosly grouping (just for display and debugging)
///  - Will look for any GO's with a 'StateBase' inherited component. If it's parent DOESN'T have any state components it's assumed to be a group for display
///  - All state go's should be active when entering play mode
///  - Keeps track of all active states if you need to know
/// 
///  For state labels create a GO named 'State label' anywhere. Set the icon (top left in inspector) to a long text label
///</summary>
/// 
/// TODO: Remove hack for adding GO's to refresh inspector window in build version
/// 
public class AllStatesManager : StateGroup // CHECK: Don't like having to inherit from StateGroup here. This is for ungrouped states
{
	// Allows multiple states active
	//public List<StateGroup> groups;
	public List<StateManager> initialStates; // Don't drag-drop this. It's set within each StateManager
	//public List<StateGroup> allGroups;
	public List<StateManager> allStates;
	public List<StateManager> currentStates;

	public IdentityBase rootIdentity;
	//string currentRunningStatesLabelDebug = "";

	public string windowTitle = "Inspector";

	public Rect rect = new Rect(10, 14, 150, 120);
	public float labelWidth = 120;
	public GUISkin labelSkin;
	public float labelLineHeight = 11.5f;

	public void Start() // Done in start not awake to let others subscribe to events before the GO is disabled
	{
		labelSkin = (GUISkin)Resources.Load("Skins/Label", typeof(GUISkin));


		initialStates.Clear(); // Just to make sure no-one is initialising states the wrong way (use checkbox in each individual state)

		if (GetComponent<IdentityBase>())
			Debug.LogWarning("Don't include an 'IdentityBase' component on the same GO as the StateManager. Put it in a parent GO");

		rootIdentity = ToolBox.GetComponentInParents<IdentityBase>(transform.parent);
		if (rootIdentity == null)
			Debug.LogWarning("AllStateManager can't find it's root identity. Add 'IdentityBase' component to the most parent GO", this);

		// Disable all children states to make sure (you could manually disable them in inspector)
		foreach (StateManager stateManager in transform.GetComponentsInChildren<StateManager>())
		{
			// This has been tagged as a startup state
			if (stateManager.initialState)
				initialStates.Add(stateManager);

			allStates.Add(stateManager);

			// TODO: Sort alphabetically. At the moment it's sorted by creation order NOT name
			//allStates.Sort()

			stateManager.OnStateEnter += StateManagerOnStateEnter;
			stateManager.OnStateExit += StateManagerOnStateExit;

			// Check: Why did I have this here? Calling exit on initialiation seems weird...
			//stateManager.InvokeExitState();

			// CHECK: Don't need this line because it's in ExitState, but just in case the go's were disabled (so they wouldn't get the ExitState function call)
			stateManager.gameObject.SetActive(false);
		}

		// Bail if no states found
		if (allStates.Count == 0)
		{
			Debug.LogWarning("No states set! StateMachine : " + transform.root.name, this);
			return;
		}

		// No intial state set! Just pick the first one and give a warning
		if (initialStates.Count == 0)
		{
			Debug.LogWarning("No initial state set! StateMachine : " + transform.root.name, this);
			return;
		}

		// Bail if initialState is null
		//		if(initialStates[0] == null)
		//		{
		//			Debug.LogWarning("Initial state is null! StateMachine : " + transform.root.name, this);
		//			return;
		//		}

		// Initialise. Allows multiple states active
		foreach (StateManager initialState in initialStates)
			// All states should be disabled at first TODO: disable all the states here beforehand?
			// So activate the initial one
			if (initialState != null)
			{
				initialState.gameObject.SetActive(true);
				// Tell the first state to activate itself for any custom setup/initialisation
				initialState.EnterState();
			}
	}

	// Manually call update. Safer than letting Unity auto update stuff when disabling GO's, plus saves reflection calls from Unity
	public void Update()
	{
		for (int i = 0; i < currentStates.Count; i++)
			if (currentStates != null)
				currentStates[i].ManualUpdate();
	}

	// Manually call fixedupdate. Safer than letting Unity auto update stuff when disabling GO's, plus saves reflection calls from Unity
	public void FixedUpdate()
	{
		for (int i = 0; i < currentStates.Count; i++)
			if (currentStates != null)
				currentStates[i].ManualFixedUpdate();
	}

	/*// Debug state display
	public void OnGUI()
	{
		float widestLabel = 0;

		if (Camera.main == null)
			return;

		GUI.skin = labelSkin;
		GUILayout.BeginArea(rect, "", "box");
		//GUILayout.BeginArea(new Rect(10, 14, 150, 120), "things", "box");
		if (rootIdentity != null)
		{
			Vector2 pos = Camera.main.WorldToScreenPoint(rootIdentity.transform.position);

			/*		foreach(var stateManager in currentStates)
					{
						if(stateManager.name.Length< 5)
							Debug.Log("YAY");
					}

					// Find max width word
					int highest = 0;
					for (int i = 0; i < currentStates.Count; i++)
					{
						if (currentStates[i].groupParent.name.Length > highest)
							highest = currentStates[i].groupParent.name.Length;
					}
				

			for (int i = 0; i < currentStates.Count; i++)
				if (currentStates != null)
				{
					rect.x = pos.x;
					rect.y = Screen.height - pos.y;
//					GUILayout.BeginHorizontal();
//					if(currentStates.Count > 1)
//					{
//						labelWidth = currentStates[i].groupParent.name.Length;
//						GUILayout.Label(currentStates[i].groupParent.name, GUILayout.Width(labelWidth));
//					}
					labelWidth = currentStates[i].name.Length;
					if(labelWidth > widestLabel)
						widestLabel = labelWidth;
					GUILayout.Label(currentStates[i].name, GUILayout.Width(labelWidth));
//					GUILayout.EndHorizontal();
				}
		}
		GUILayout.EndArea();

		rect.width = 4 + widestLabel*6;
		rect.height = currentStates.Count * labelLineHeight+6;
	}*/


	// Dynamically add and remove the current running states to keep track of them from one spot
	// Not really actively used much, mainly just for quick reference and debugging controls
	void StateManagerOnStateEnter(Object sender)
	{
		DebugManager.Log("StateManager: OnStateEnter called from SSM", this, 2);
		HackForceInspectorRefresh();
		currentStates.Add(sender as StateManager);
	}

	void StateManagerOnStateExit(Object sender)
	{
		DebugManager.Log("StateManager: OnStateExit called from SSM", this, 2);
		HackForceInspectorRefresh();
		currentStates.Remove(sender as StateManager);
	}

	void HackForceInspectorRefresh()
	{
		// TODO see how to use             EditorUtility.SetDirty(context.GameObj); //this updates Inspector GUI
		GameObject hackGO = new GameObject("Hack to force refresh");
		Destroy(hackGO);
	}

	public void ExitAllRunningStates()
	{
		foreach (StateManager stateManager in currentStates)
			stateManager.ExitState();

		currentStates.Clear();
	}

	bool IsStateActive(StateManager state)
	{
		return state.enabled;
	}
}
