using UnityEngine;
/// <summary>
/// - Inherit components from this for convienience if you want to listen to the state events
/// - It'll find the parent GO with any component inherited from 'IdentityBase' for convienience
/// - It'll find the master StateManager
/// - Use 'override' in your components to implement the functions
/// * WARNING you CAN NOT use a normal old 'Awake' function, you must override this one or nothing will work
/// CHECK: Should I use a different name? Because I've been caught with this. The root vars may/may not be avaliable to a custom Awake function though, which is very dangerous..
/// </summary>
public class StateBase : MonoBehaviour
{
	// Only set this if this component isn't in the same GO as it
	protected StateManager stateManager;
	protected AllStatesManager allStatesManager;
	protected IdentityBase rootIdentity;
	protected StateGroup stateGroup;

	public bool enabledDebug = true;

	public virtual void Awake()
	{
		// Find an identity type presuming this object has one for identification
		rootIdentity = ToolBox.GetComponentInParents<IdentityBase>(transform.parent);
		if(!rootIdentity)
			DebugManager.Log("Root Identity not found : ", this, 1);
		allStatesManager = ToolBox.GetComponentInParents<AllStatesManager>(transform.parent);
		stateGroup = ToolBox.GetComponentInParents<StateGroup>(transform.parent);

		DebugManager.Log("Awake : " + this, this, 2);

		stateManager = GetComponent<StateManager>();

		if (enabledDebug)
		{
			stateManager.OnStateEnter += OnStateEnter;
			stateManager.OnStateEnterLate += OnStateEnterLate;
			stateManager.OnStateExiting += OnStateExiting;
			stateManager.OnStateExit += OnStateExit;
			stateManager.OnStateUpdate += OnStateUpdate;
			stateManager.OnStateFixedUpdate += OnStateFixedUpdate;
		}
	}

	public virtual void OnStateEnter(Object sender)
	{
		DebugManager.Log(rootIdentity.name + ": OnStateEnter : " + this, this, 2);

	}

	public virtual void OnStateEnterLate(Object sender)
	{
		DebugManager.Log(rootIdentity.name + ": OnStateEnterLate : " + this, this, 2);
	}

	public virtual void OnStateUpdate(Object sender)
	{
		// CHECK TODO: Replace this with a #define to remove it from runtime
		DebugManager.Log(rootIdentity.name + ": OnStateUpdate : " + this, this, 5);
	}

	public virtual void OnStateFixedUpdate(Object sender)
	{
		// CHECK TODO: Replace this with a #define to remove it from runtime
		DebugManager.Log(rootIdentity.name + ": OnStateUpdate : " + this, this, 5);
	}

	public virtual void OnStateExiting(Object sender)
	{
		DebugManager.Log(rootIdentity.name + ": OnStateExiting : " + this, this, 5);
	}

	public virtual void OnStateExit(Object sender)
	{
		DebugManager.Log(rootIdentity.name + ": OnStateExit : " + this, this, 5);

	/*	stateManager.OnStateEnter -= OnStateEnter;
		stateManager.OnStateEnterLate -= OnStateEnterLate;
		stateManager.OnStateExiting -= OnStateExiting;
		stateManager.OnStateExit -= OnStateExit;
		stateManager.OnStateUpdate -= OnStateUpdate;
	*/
	}
}