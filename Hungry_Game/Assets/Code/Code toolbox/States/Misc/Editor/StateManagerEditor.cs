﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof (StateManager))]
public class StateManagerEditor : Editor
{

	public override void OnInspectorGUI()
	{
		//DrawDefaultInspector();
		//EditorGUIUtility.LookLikeControls();

		StateManager singleStateManager = (StateManager) target;
		if (!singleStateManager.gameObject)
			return;

		singleStateManager.initialState = EditorGUILayout.Toggle("Auto start", singleStateManager.initialState);
		//singleStateManager.alwaysActive = EditorGUILayout.Toggle("Always active?", singleStateManager.alwaysActive);
		singleStateManager.nextState = (StateManager)EditorGUILayout.ObjectField("Next state", singleStateManager.nextState, typeof(StateManager), true);

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("Debug controls");
		AllStatesManagerEditor.DrawButtons(singleStateManager, false, false);
		
		EditorGUILayout.EndHorizontal();
		/*
		if (GUILayout.Button("Force transition"))
		{
			stateManager.TransitionToNextState();
		}
		if (GUILayout.Button("Force enter state"))
		{
			stateManager.ChangeState(stateManager); // Change state to itself. Even if it's the same as the current state
		}
		 */
		if (GUI.changed)
			EditorUtility.SetDirty(singleStateManager);

	}
}