using UnityEngine;

public struct TouchContainer
{
	public Vector2 deltaPosition;
	public float deltaTime;
	public int fingerId;
	public TouchPhase phase;
	public Vector2 screenPosition;
	public Vector3 worldPosition;
	public int tapCount;
	// Non built-in touch info
	public Vector2 velocity;
	public RaycastHit raycastHit;
}

public class TouchController : MonoBehaviour
{
	public float touchWorldDepth = 10;
	RaycastHit rayInfo;
	Ray ray;
	TouchContainer touchContainer;

	Vector2 lastPosition; // Only used for mouse since the Unity touch code doesn't fire Move events if you didn't move

	void OnDrawGizmos()
	{
		Vector3 p = ScreenToWorldPoint();
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(p, 0.5F);


	}

	void Update()
	{
		// Touch screen
		if(Input.touchCount > 0)
			for(int i = 0; i < Input.touchCount; i++)
			{
				touchContainer.fingerId = Input.touches[i].fingerId;
				touchContainer.screenPosition = Input.touches[i].position;
				touchContainer.worldPosition = ScreenToWorldPoint();
				touchContainer.tapCount = Input.touches[i].tapCount;
				touchContainer.raycastHit = TouchRayCast(touchContainer.screenPosition);

				switch(Input.touches[i].phase)
				{
					case TouchPhase.Began:
						touchContainer.phase = TouchPhase.Began;
						TouchControllerEvents.InvokeTouchControllerBegan(touchContainer);
						break;
					case TouchPhase.Ended:
						touchContainer.phase = TouchPhase.Ended;
						TouchControllerEvents.InvokeTouchControllerEnded(touchContainer);
						break;
					case TouchPhase.Moved:
						touchContainer.phase = TouchPhase.Moved;
						TouchControllerEvents.InvokeTouchControllerMoving(touchContainer); // TODO touch velocity
						break;
					case TouchPhase.Stationary:
						touchContainer.phase = TouchPhase.Stationary;
						TouchControllerEvents.InvokeTouchControllerStationary(touchContainer);
						break;
				}
			}
		else
		{
			// Mouse TODO: Use compile #if's to remove mouse code for touch devices? What about Android tablets with a mouse, do they register as touch still?
			touchContainer.fingerId = 0;
			touchContainer.screenPosition = Input.mousePosition;
			touchContainer.worldPosition = ScreenToWorldPoint();
			touchContainer.tapCount = 0;
			touchContainer.raycastHit = TouchRayCast(touchContainer.screenPosition);
			
			if(Input.GetMouseButtonDown(0))
			{
				touchContainer.phase = TouchPhase.Began;
				TouchControllerEvents.InvokeTouchControllerBegan(touchContainer);
			}
			else if(Input.GetMouseButtonUp(0))
			{
				touchContainer.phase = TouchPhase.Ended;
				TouchControllerEvents.InvokeTouchControllerEnded(touchContainer);
			}

			// Moving
			if(lastPosition != touchContainer.screenPosition) // Check actual movement
			{
				touchContainer.phase = TouchPhase.Moved;
				TouchControllerEvents.InvokeTouchControllerMoving(touchContainer); // TODO touch velocity				
				lastPosition = touchContainer.screenPosition;
			}
			else
			{
				touchContainer.phase = TouchPhase.Stationary;
				TouchControllerEvents.InvokeTouchControllerStationary(touchContainer);
			}
		}
	}

	Vector3 ScreenToWorldPoint()
	{
		return Camera.main.ScreenToWorldPoint(new Vector3(touchContainer.screenPosition.x, touchContainer.screenPosition.y, touchWorldDepth));
	}

	RaycastHit TouchRayCast(Vector2 screenPosition)
	{
		ray = Camera.main.ScreenPointToRay(screenPosition); // TODO support multitouch
		if(DebugManager.debugLevel > 2)
			Debug.DrawRay(ray.origin, ray.direction, Color.red, 1);
		Physics.Raycast(ray, out rayInfo);

		DebugManager.Log("Hit : " + rayInfo.collider.name, this, 4);

		return rayInfo;
	}
}
