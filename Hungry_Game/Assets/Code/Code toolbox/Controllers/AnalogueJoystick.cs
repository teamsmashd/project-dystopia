using UnityEngine;

public class AnalogueJoystick : MonoBehaviour
{
	public Vector3 position;

	public string horizontalAxisName;
	public string verticalAxisName;

	// Update is called once per frame
	void FixedUpdate()
	{
		position.x = Input.GetAxisRaw(horizontalAxisName);
		position.y = Input.GetAxisRaw(verticalAxisName);
	}
}
