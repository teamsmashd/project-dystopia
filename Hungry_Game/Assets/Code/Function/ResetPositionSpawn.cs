﻿using UnityEngine;
using System.Collections;

public class ResetPositionSpawn : MonoBehaviour 
{
	public GameObject resetObject;
	public GameObject resetPosition;

	public void ResetPosition()
	{
		resetObject.transform.position = resetPosition.transform.position;
		resetObject.transform.parent = null;
	}
}
