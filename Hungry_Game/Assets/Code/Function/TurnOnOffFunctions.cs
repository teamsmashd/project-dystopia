﻿using UnityEngine;
using System.Collections;

public class TurnOnOffFunctions : MonoBehaviour 
{
	public GameObject ThingToTurnOnOff;

	public void TurnOn()
	{
		ThingToTurnOnOff.SetActive(true);
	}

	public void TurnOff()
	{
		ThingToTurnOnOff.SetActive(false);
	}
}
