﻿using UnityEngine;
using System.Collections;

public class LerpPositionV3 : MonoBehaviour 
{
	public GameObject mainObject;
	public Vector3 currentPosition;
	public Vector3 currentOffSet;
	public Vector3 startOffSet;
	public Vector3 finishOffSet;
	public float timer;

	public void Update()
	{
		currentPosition = currentOffSet + mainObject.transform.parent.position;
	}

	public void LerpPositionForward()
	{
		mainObject.transform.position = currentPosition;
		currentOffSet = Vector3.MoveTowards(currentOffSet, finishOffSet, timer * Time.deltaTime);
	}

	public void LerpPositionBackwards()
	{
		mainObject.transform.position = currentPosition;
		currentOffSet = Vector3.MoveTowards(currentOffSet, startOffSet, timer * Time.deltaTime);
	}
}
