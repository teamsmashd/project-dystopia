﻿using UnityEngine;
using System.Collections;

public class TurnOnAnimator : MonoBehaviour 
{
	public Animator AniTurnOn;

	public void TurnOnAni()
	{
		AniTurnOn.enabled = true;
	}
}
