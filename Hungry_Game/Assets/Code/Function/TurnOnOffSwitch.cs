﻿using UnityEngine;
using System.Collections;

public class TurnOnOffSwitch : MonoBehaviour 
{
	public GameObject gameObject1;
	public GameObject gameObject2;

	public void turnOnGameObject1()
	{
		gameObject1.SetActive(true);
		gameObject2.SetActive(false);
	}

	public void turnOnGameObject2()
	{
		gameObject1.SetActive(false);
		gameObject2.SetActive(true);
	}
}
