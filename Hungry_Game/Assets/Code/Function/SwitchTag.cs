﻿using UnityEngine;
using System.Collections;

public class SwitchTag : MonoBehaviour 
{
	public GameObject mainObject;
	public string tagChangeTo;
	public string tagChangeBack;

	public void ChangeTagTo()
	{
		mainObject.tag = tagChangeTo;
	}

	public void ChangeTagBack()
	{
		mainObject.tag = tagChangeBack;
	}
}
