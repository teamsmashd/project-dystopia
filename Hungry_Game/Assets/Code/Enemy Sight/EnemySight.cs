﻿using UnityEngine;
using System.Collections;

public class EnemySight : MonoBehaviour 
{
	public TriggerBool triggerBool;
	public TriggerBool secondTriggerBool;
	public TriggerRespawn triggerRespawn;

	public string tagged;

	public void Update()
	{
		if(triggerBool.inTrigger || secondTriggerBool.inTrigger)
		{
			triggerRespawn.tagged = null;
			triggerBool.inTrigger = true;
		}

		else 
		{
			triggerRespawn.tagged = tagged;
		}
	}
}
