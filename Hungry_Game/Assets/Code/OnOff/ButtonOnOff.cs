﻿using UnityEngine;
using System.Collections;

public class ButtonOnOff : MonoBehaviour 
{
	public GameObject onOffGameObject;
	public KeyCode activationButton;

	public void Update()
	{
		if(Input.GetKeyDown(activationButton))
		{
			onOffGameObject.SetActive(true);
		}

		if(Input.GetKeyUp(activationButton))
		{
			onOffGameObject.SetActive(false);
		}
	}
}
