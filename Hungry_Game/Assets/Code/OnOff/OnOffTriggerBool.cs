﻿using UnityEngine;
using System.Collections;

public class OnOffTriggerBool : MonoBehaviour 
{
	public TriggerBool triggerBool;
	public GameObject objectOnOff;

	public void Update()
	{
		if(triggerBool.inTrigger)
		{
			objectOnOff.SetActive(false);
		}

		else 
		{
			objectOnOff.SetActive(true);
		}
	}
}
