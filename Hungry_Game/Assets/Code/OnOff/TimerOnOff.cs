﻿using UnityEngine;
using System.Collections;

public class TimerOnOff : MonoBehaviour 
{
	public GameObject thingToTurnOnOff;
	public float countDown;
	public float currentCountDown;
	public bool onOff;

	public void Awake()
	{
		currentCountDown = countDown;
	}

	public void Update()
	{
		currentCountDown -= 1 * Time.deltaTime;
		if(currentCountDown <= 0)
		{
			if(onOff)
			{
				thingToTurnOnOff.SetActive(true);
			}
			
			else
			{
				thingToTurnOnOff.SetActive(false);
			}

			currentCountDown = countDown;
		}
	}
}
