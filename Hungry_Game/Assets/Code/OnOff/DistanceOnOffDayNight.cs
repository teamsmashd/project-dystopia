﻿using UnityEngine;
using System.Collections;

public class DistanceOnOffDayNight : MonoBehaviour 
{
	public GameObject object1;
	public GameObject object2;
	public GameObject objectOnOff;
	public float maxDistance;
	public bool onOff;
	private string tagged;
	public string dayTag;
	public string nightTag;
	private float distance;

	public BoolDayNight dayNight;
	
	public void Awake()
	{
		if(dayNight.dayTime)
		{
			tagged = dayTag;
		}

		else
		{
			tagged = nightTag;
		}

		object2 = GameObject.FindGameObjectWithTag(tagged);
	}
	
	public void Update()
	{
		distance = Vector3.Distance(object1.transform.position, object2.transform.position);
		if(distance >= maxDistance)
		{
			objectOnOff.SetActive(onOff);
		}
	}
}
