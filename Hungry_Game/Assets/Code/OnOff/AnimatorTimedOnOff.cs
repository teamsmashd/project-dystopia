﻿using UnityEngine;
using System.Collections;

public class AnimatorTimedOnOff : MonoBehaviour 
{
	public Animator AniTurnOn;
	public float timer;
	public float startTimer;
	public bool ifOnOff;
	public bool onOff;

	public void Awake()
	{
		timer = startTimer;
	}

	public void Update()
	{
		if(AniTurnOn.enabled == ifOnOff)
		{
			timer -= 1 * Time.deltaTime;
			if(timer <= 0)
			{
				AniTurnOn.enabled = onOff;
				timer = startTimer;
			}
		}
	}
}
