﻿using UnityEngine;
using System.Collections;

public class DistanceOnOff : MonoBehaviour 
{
	public GameObject object1;
	public GameObject object2;
	public GameObject objectOnOff;
	public float maxDistance;
	public bool onOff;
	public string findObjectName;
	private float distance;

	public void Awake()
	{
		object2 = GameObject.Find(findObjectName);
	}

	public void Update()
	{
		distance = Vector3.Distance(object1.transform.position, object2.transform.position);
		if(distance >= maxDistance)
		{
			objectOnOff.SetActive(onOff);
		}
	}
}
