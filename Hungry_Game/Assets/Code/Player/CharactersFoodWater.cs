﻿using UnityEngine;
using System.Collections;

public class CharactersFoodWater : MonoBehaviour 
{
	public float foodHealth;
	public float waterHealth;
	public float maxHealth;
	public float drainingRate;
	public float drainingRunRate;
	private PlayerMovement playerMovement;
	
	public event EventHandlers.MinimalHandler DeathEvent = delegate { };
	
	public void Awake()
	{
		foodHealth = maxHealth;
		waterHealth = maxHealth;
		playerMovement = GameObject.FindObjectOfType(typeof(PlayerMovement)) as PlayerMovement;
	}

	public void ResetHealth()
	{
		foodHealth = maxHealth;
		waterHealth = maxHealth;
	}
	
	public void Update()
	{
		if(foodHealth >= maxHealth)
		{
			foodHealth = maxHealth;
		}
		
		if(waterHealth >= maxHealth)
		{
			waterHealth = maxHealth;
		}

		if(playerMovement.currentSpeed >= 1.04f)
		{
			foodHealth -= drainingRunRate * Time.deltaTime;
			waterHealth -= drainingRunRate * Time.deltaTime;
		}

		else
		{
			foodHealth -= drainingRate * Time.deltaTime;
			waterHealth -= drainingRate * Time.deltaTime;
		}
		
		if(foodHealth <= 0 || waterHealth <= 0)
		{
			foodHealth = maxHealth;
			waterHealth = maxHealth;
			DeathEvent();
		}
	}
}
