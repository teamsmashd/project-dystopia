﻿using UnityEngine;
using System.Collections;

public class AwakeFindPlayer : MonoBehaviour 
{
	public GameObject playerObject;
	public string tagged;

	public void Awake()
	{
		playerObject = GameObject.FindGameObjectWithTag(tagged);
	}
}
