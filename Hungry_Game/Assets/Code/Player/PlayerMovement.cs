﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{

	public Rigidbody mainBody;
	
	public float currentSpeed;
	public float acceleration;
	public float walkSpeed;
	public float runSpeed;
	private float maxSpeed;
	public float rotationSpeed;

	public Vector3 playerVector;

	public KeyCode walkForwardKey;
	public KeyCode walkBackwardKey;
	public KeyCode walkLeftKey;
	public KeyCode walkRightKey;
	public KeyCode runKey;

	public Animator animator;

	public void Awake()
	{
		maxSpeed = walkSpeed;
	}

	void Update() 
	{
		mainBody.transform.forward = playerVector;

		float currentSpeedZ = mainBody.velocity.z;
		float currentSpeedX = mainBody.velocity.x;

		currentSpeedX = Mathf.Pow(currentSpeedX, 2);
		currentSpeedX = Mathf.Sqrt(currentSpeedX);

		currentSpeedZ = Mathf.Pow(currentSpeedZ, 2);
		currentSpeedZ = Mathf.Sqrt(currentSpeedZ);

		float totalSpeed = currentSpeedX + currentSpeedZ;
		
		currentSpeed = currentSpeedX + currentSpeedZ;
		currentSpeed = Mathf.Pow(currentSpeed, 2);
		currentSpeed = Mathf.Sqrt(currentSpeed)/walkSpeed;
		animator.SetFloat("Speed", currentSpeed);

		if(Input.GetKey(walkForwardKey) || Input.GetKey(walkBackwardKey) || Input.GetKey(walkRightKey) || Input.GetKey(walkLeftKey))
		{
			if(Input.GetKey(walkForwardKey))
			{
				if(playerVector.z <= 1)
				{
					playerVector.z += rotationSpeed * Time.deltaTime;
				}
			}

			else
				if(Input.GetKey(walkBackwardKey))
			{
				if(playerVector.z >= -1)
				{
					playerVector.z -= rotationSpeed * Time.deltaTime;
				}
			}

			else
			{
				playerVector.z = Mathf.MoveTowards(playerVector.z, 0, rotationSpeed * Time.deltaTime);
			}

				if(Input.GetKey(walkRightKey))
			{
				if(playerVector.x <= 1)
				{
					playerVector.x += rotationSpeed * Time.deltaTime;
				}
			}
			
			else
				if(Input.GetKey(walkLeftKey))
			{
				if(playerVector.x >= -1)
				{
					playerVector.x -= rotationSpeed * Time.deltaTime;
				}
			}
			
			else
			{
				playerVector.x = Mathf.MoveTowards(playerVector.x, 0, rotationSpeed * Time.deltaTime);
			}

			if(Input.GetKey(walkForwardKey) || Input.GetKey(walkBackwardKey) || Input.GetKey(walkRightKey) || Input.GetKey(walkLeftKey))
			{
				if(totalSpeed <= maxSpeed && totalSpeed >= -maxSpeed)
				{
					mainBody.AddForce(transform.TransformDirection((Vector3.forward * acceleration) * Time.deltaTime));
				}
			}

			if(Input.GetKey(runKey))
			{
				maxSpeed = runSpeed;
			}
			
			else
			{
				maxSpeed = walkSpeed;
			}
		}
	}
}
