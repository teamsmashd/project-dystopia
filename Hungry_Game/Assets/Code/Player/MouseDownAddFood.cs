﻿using UnityEngine;
using System.Collections;

public class MouseDownAddFood : MonoBehaviour 
{
	public CharactersFoodWater foodWater;
	public FoodCounter foodCounter;
	public float addFoodAmount;

	public void Awake()
	{
		foodWater = GameObject.FindObjectOfType(typeof(CharactersFoodWater)) as CharactersFoodWater;
		foodCounter = GameObject.FindObjectOfType(typeof(FoodCounter)) as FoodCounter;
	}

	public void OnMouseDown() 
	{
		if(foodCounter.foodAmount > 0)
		{
			foodWater.foodHealth += addFoodAmount;
			foodCounter.foodAmount --;
		}

		else
		{
			return;
		}
	}
}
