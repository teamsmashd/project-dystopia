﻿using UnityEngine;
using System.Collections;

public class MouseDownAddWater : MonoBehaviour 
{
	public CharactersFoodWater foodWater;
	public WaterCounter waterCounter;
	public float addWaterAmount;
	
	public void Awake()
	{
		foodWater = GameObject.FindObjectOfType(typeof(CharactersFoodWater)) as CharactersFoodWater;
		waterCounter = GameObject.FindObjectOfType(typeof(WaterCounter)) as WaterCounter;
	}
	
	public void OnMouseDown() 
	{
		if(waterCounter.waterAmount > 0)
		{
			foodWater.waterHealth += addWaterAmount;
			waterCounter.waterAmount --;
		}
		
		else
		{
			return;
		}
	}
}
